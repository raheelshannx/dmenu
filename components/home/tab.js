import React from "react";
import { useSelector } from "react-redux";
import ProductItem from "./productitem";

function Tab(props) {
  const languages = useSelector((state) => state.language);
  const filtered = languages.filter((item) => item.isDefault);
  const lang = filtered[0];

  let id = `tab-${props.category.id}`;
  let classes = `tab-pane ${props.index == 0 ? "active" : ""}`;
  return (
    <div role="tabpanel" className={classes} id={id} key={id}>
      <legend className="text-center">{props.category[lang.short_name]}</legend>

      {props.products.map((product) => {
        return <ProductItem {...product} key={product.product_id} />;
      })}
    </div>
  );
}

export default Tab;