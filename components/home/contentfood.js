import React from 'react';
import Feedback from './feedback';
import Foodcard from './foodcard';
import Title from './title';

function ContentFood(props) {
    return <div className="contentFood">
        <Feedback lang={props.lang}/>
        <Title />
        <Foodcard categories={props.categories} products={props.products}/>
    </div>;
}

export default ContentFood;