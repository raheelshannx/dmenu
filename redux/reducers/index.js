import { combineReducers } from 'redux';

import languageReducer from './languageReducer';
import productReducer from './productReducer';
import categoryReducer from './categoryReducer';
import storeReducer from './storeReducer';

export default combineReducers({
    language: languageReducer,
    products: productReducer,
    categories: categoryReducer,
    store: storeReducer,
});