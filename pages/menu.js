import Head from "next/head";

import ContentFood from "../components/home/contentfood";
import FeedBackModal from "../components/home/feedbackmodal";
import Footer from "../components/home/footer";
import Theme from "../components/home/theme";
import Detail from "../components/detail";
import { useRouter } from "next/router";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { useState, useEffect } from "react";
import { addLanguage } from "../redux/actions/languageAction";
import { addProduct } from "../redux/actions/productAction";
import { addCategory } from "../redux/actions/categoryAction";
import { setStore } from "../redux/actions/storeAction";

export default function Menu() {
  const router = useRouter();
  const { store_id, outlet_id } = router.query;
  const url = `https://services.nextaxe.com/api/menu?store_id=${store_id}&outlet_id=${outlet_id}`;
  const products = useSelector((state) => state.products);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (products == null || products.length == 0) {
      getData();
    }
  });

  const getData = () => {
    setLoading(true);
    axios.get(url).then((resp) => {
      const response = resp.data;
      if (response != null || response != undefined) {
        if (response.products.length > 0) {
          response.products.map((product) => {
            dispatch(addProduct(product));
          });
        }
        if (response.categories.length > 0) {
          response.categories.map((category) => {
            dispatch(addCategory(category));
          });
        }
        if (response.languages.length > 0) {
          response.languages.map((language, index) => {
            language.isDefault = index == 0 ? true : false;
            dispatch(addLanguage(language));
          });
        }
        if (response.store != null) {
          dispatch(setStore(response.store));
        }
        setLoading(false);
      }
    });
  };

  const renderData = () => {
    return (
      <div>
        <main role="main" className="main_wrapper">
          <div className="contentMenuWrapper">
            {loading ? (
              <div className="loader">Loading...</div>
            ) : (
              <ContentFood />
            )}
          </div>
        </main>
        {/* <FeedBackModal /> */}

        <Footer />
        <Theme />
        <Detail />
      </div>
    );
  };

  return <>{renderData()}</>;
}
