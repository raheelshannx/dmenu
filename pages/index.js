import Head from "next/head";

import ContentFood from "../components/home/contentfood";
import FeedBackModal from "../components/home/feedbackmodal";
import Footer from "../components/home/footer";
import Theme from "../components/home/theme";
import useFetch from "../service/useFetch";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addLanguage } from "../redux/actions/languageAction";
import { addProduct } from "../redux/actions/productAction";
import { addCategory } from "../redux/actions/categoryAction";
import { setStore } from "../redux/actions/storeAction";
import { useRouter } from "next/router";
import Link from "next/link";
import axios from "axios";
import Geocode from "react-geocode";

export default function Home() {
  const dispatch = useDispatch();
  const router = useRouter();
  const { store_id } = router.query;
  const url = `https://services.nextaxe.com/api/outlets?store_id=${store_id}`;
  const [response, loading, hasError] = useFetch(url, store_id);
  const [branch, setBranch] = useState("");
  const [branches, setBranches] = useState([]);
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");

  useEffect(() => {
    if (response != null && response != undefined && Array.isArray(response)) {
      setBranches(response);
    }
    console.log(response);
  });
  const onSelectBranch = (event) => {
    let url = `https://services.nextaxe.com/api/menu?store_id=${store_id}&outlet_id=${event.target.value}`;
    setBranch(event.target.value);

    axios.get(url).then((resp) => {
      const response = resp.data;
      if (response != null || response != undefined) {
        if (response.products.length > 0) {
          response.products.map((product) => {
            dispatch(addProduct(product));
          });
        }
        if (response.categories.length > 0) {
          response.categories.map((category) => {
            dispatch(addCategory(category));
          });
        }
        if (response.languages.length > 0) {
          response.languages.map((language, index) => {
            language.isDefault = index == 0 ? true : false;
            dispatch(addLanguage(language));
          });
        }
        if (response.store != null) {
          dispatch(setStore(response.store));
        }
      }
    });
  };

  const onNameChange = (event) => {
    setName(event.target.value);
  };
  const onPhoneChange = (event) => {
    setPhone(event.target.value);
  };
  const handleSubmit = (event) => {
    if (branch == 0) {
      alert("Please select a Branch.");
      event.preventDefault();
      return;
    }
    const header = {
      name: name,
      mobile: phone
    };

    axios.post(`https://jsonplaceholder.typicode.com/users`, { header })
      .then(res => {
        console.log(res);
        console.log(res.data);
      }).catch(error => {
        console.error(error);
      });
  };
  const getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition(function (position) {
        console.log("Latitude is :", position.coords.latitude);
        console.log("Longitude is :", position.coords.longitude);
        
        Geocode.enableDebug();
        // Get address from latidude & longitude.
        Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
          response => {
            const address = response.results[0].formatted_address;
            console.log(address);
          },
          error => {
            console.error(error);
          }
        );
      });
    }
  }
  const renderData = (response) => {
    const url = `/menu?store_id=${store_id}&outlet_id=${branch}`;
    return (
      <main role="main" className="main_wrapper">
        <div className="contentMenuWrapper">
          <div className="contentFood">
            <div className="login">
              <form>
                <div className="form-group branch_select">
                  <span className="location_search">
                    <a onClick={getLocation} href="#">
                      <img src="assets/img/location.svg" />
                    </a>
                  </span>
                  <label>Select Branch <em>*</em></label>
                  <select className="custom-select"
                    onChange={onSelectBranch}
                    value={branch}
                  >
                    <option value={0}>
                      Select Branch
                </option>
                    {branches.map((branch) => {
                      return (
                        <option
                          key={branch.id.toString()}
                          value={branch.id}
                        >
                          {branch.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="form-group">
                  <label>NAME</label>
                  <input
                    className="form-control"
                    type="text"
                    value={name}
                    onChange={onNameChange}
                    placeholder="Your Name"
                  />
                </div>
                <div className="form-group">
                  <label>Mobile Number</label>
                  <input
                    className="form-control"
                    type="number"
                    value={phone}
                    onChange={onPhoneChange}
                    placeholder="Phone No."
                  />
                </div>
                <div className="form-group submitbtn">
                  <Link href={url}>
                    <button onClick={handleSubmit}>
                      GO <i><img src="assets/img/arrow.jpg" /></i>
                    </button>
                  </Link>
                </div>

              </form>
            </div>
          </div>
        </div>
      </main>
    );
  };

  //   return (
  //     <>
  //       {loading ? (
  //         <div className="loader">Loading...</div>
  //       ) : hasError ? (
  //         <div className="loader">
  //           Error occured. Please refresh the page to try again
  //         </div>
  //       ) : (
  //         renderData(response)
  //       )}
  //     </>
  //   );

  return <>{renderData(response)}</>;
}
